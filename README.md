# Padronização de codificação PHP

## Regras

- Código PHP delimitado somente com a tag longa **<?php** ou a tag curta de echo **<?=**.

- A declaração da namespace deve ser seguida de uma linha em branco.

- A utilização da declaração “**use”** deve ocorrer depois da declaração do **namespace** e deve ser de uma declaração por linha.

- Depois do bloco de declarações “**use”** deve-se incluir uma linha em branco.

- A tag de fechamento de código PHP (**?>**) deve ser **omitida** para arquivos contendo somente código PHP.

- Todo arquivo PHP deve terminar com uma simples linha em branco.

  | **EXEMPLO**                                                  |
  | ------------------------------------------------------------ |
  | {+<\?php +}<br />namespace Vendor\Package;<br />{+&nbsp;+}<br />{+use+} FooClass;<br />use BarClass as Bar;<br />use OtherVendor\OtherPackage\BazClass;<br />{+&nbsp;+}<br />// ... additional PHP code ...<br />{+&nbsp;+} |

  ------

- Arquivos sejam criados para declarar símbolos (classes, funções, constante, etc.) ou causar efeitos colaterais (gerar saídas, alterar configurações “.ini”, emitir erros ou exceções, etc.). Não misturar em um arquivo declaração com efeitos colaterais.

  | **NÃO CONFORME**                                             | **CONFORME**                                                 |
  | ------------------------------------------------------------ | ------------------------------------------------------------ |
  | <?php<br />// side effect: change ini settings <br />ini_set('error_reporting', E_ALL);<br /><br />// side effect: loads a file <br />include "file.php";<br /><br />// side effect: generates output <br />echo "<html>\n";<br /><br />// declaration <br />function foo() {<br />&nbsp; &nbsp; // function body <br />} | <?php<br /> // declaration <br />function foo()<br />{<br />&nbsp; &nbsp; // function body<br />}<br /><br />// conditional declaration is *not* a side effect <br />if (! function_exists('bar')) {<br />&nbsp; &nbsp; function bar()<br />&nbsp; &nbsp; {<br />&nbsp; &nbsp; &nbsp; &nbsp; // function body<br />&nbsp; &nbsp; }<br />} |

  ------

- Cada **classe** (**interface** ou **trait**) deve ser declarada em **um arquivo somente** para ela, sem nenhuma outra declaração de classe ou função.

  | **NÃO CONFORME**                                             | **CONFORME**                                                 |
  | ------------------------------------------------------------ | ------------------------------------------------------------ |
  | ----- FILE: class.classname.php -----<br /><?php<br /><br />class ParentClass {<br />&nbsp; &nbsp; // constants, properties, methods <br />}<br /><br />class ClassName extends ParentClass {<br />&nbsp; &nbsp; // constants, properties, methods <br />} | ----- FILE: class.parentclass.php -----<br /><?php<br /><br />class ParentClass {<br />&nbsp; &nbsp; // constants, properties, methods <br />}<br /><br />----- FILE: class.classname.php -----<br /><?php<br /><br />class ClassName extends ParentClass {<br />&nbsp; &nbsp; // constants, properties, methods <br />} |

  ------

- As palavras-chave **extends** e **implements** devem ser declaradas na mesma linha da classe.

  ------

- A **chave de abertura da classe** (“**{**“) deve vir sozinha na linha logo abaixo a declaração da classe. E a **chave de fechamento da classe** (“**}**“) dever vir na próxima linha após o corpo da classe sem nenhuma linha em branco entre elas.

  ------

- Listas de **implements** podem ser divididas em várias linhas, onde cada linha subsequente é recuada uma vez. Ao fazer isso, o primeiro item da lista deve estar na linha seguinte, e deve haver apenas uma interface por linha.

  ------

- As constantes **true**, **false** e **null** devem ser usadas sempre em minúsculo.

  | **NÃO CONFORME**                                             | **CONFORME**                                                 |
  | ------------------------------------------------------------ | ------------------------------------------------------------ |
  | $valid = [- TRUE -];<br />$invalid = [- FALSE -];<br />$object = [- Null -]; | $valid = [+ true +];<br>$invalid = [+ false +];<br>$object = [+ null +]; |

  ------

- **Visibilidade** deve ser declarada para todos os **métodos**.

  | **NÃO CONFORME**                                            | **CONFORME**                                                 |
  | ----------------------------------------------------------- | ------------------------------------------------------------ |
  | [- function -] getName()<br />{<br />&nbsp;   //code<br />} | [+ public function +] getName()<br />{<br />&nbsp;   //code<br />} |

  ------

- Quando presentes, as declarações “**abstract**” e “**final**” devem preceder a declaração de visibilidade.

  | **NÃO CONFORME**                                             | **CONFORME**                                                 |
  | ------------------------------------------------------------ | ------------------------------------------------------------ |
  | [- public abstract function -] getName()<br />{<br />&nbsp;   //code<br />} | [+ abstract public function +] getName()<br />{<br />&nbsp;   //code<br />} |

  ------

- Quando presente, a declaração “**static**” deve vir depois da declaração de visibilidade.

  | **NÃO CONFORME**                                             | **CONFORME**                                                 |
  | ------------------------------------------------------------ | ------------------------------------------------------------ |
  | [- static public function -] getName()<br />{<br />&nbsp;   //code<br />} | [+ public static function +] getName()<br />{<br />&nbsp;   //code<br />} |

  ------

- Após os nomes dos **métodos** não devem haver espaço até a abertura do parêntesis.

- Não deve-se incluir espaço logo após a abertura do parêntesis da **declaração do método**, assim como antes do fechamento do parêntesis.

- Não deve existir espaço antes da vírgula de separação dos argumentos da **declaração do método**.

- Deve existir um espaço depois da vírgula de separação dos argumentos da declaração do método.

- Argumentos de métodos com valor padrão devem ficar no final da lista de argumentos da declaração do método.

- A **chave de abertura do método** (“**{**“) deve vir sozinha na linha logo abaixo a declaração do método. E a **chave de fechamento do método** (“**}**“) dever vir na próxima linha após o corpo do método sem nenhuma linha em branco entre elas.

  | **NÃO CONFORME**                                             | **CONFORME**                                                 |
  | ------------------------------------------------------------ | ------------------------------------------------------------ |
  | public static function doSomething{-&nbsp;-}({-$arg1,$arg2,$arg3-}) {-{-}<br />&nbsp; &nbsp; //code<br />}<br /><br />public static function doSomething{-&nbsp;-}({-&nbsp;-}$arg1{-&nbsp;-}, $arg2{-&nbsp;-}, $arg3{-&nbsp;-}) {-{-}<br />&nbsp; &nbsp; //code<br />} | public static function doSomething($arg1,{+&nbsp;+}$arg2,{+&nbsp;+}$arg3{+=false+})<br />{+{+}<br />&nbsp; &nbsp; //code<br />} |

  ------

- As listas de **argumentos** de um **método** podem ser divididas em várias linhas, onde cada linha subsequente é recuada uma vez. Ao fazer isso, o primeiro item da lista deve estar na linha seguinte e deve haver apenas um argumento por linha.

  | **NÃO CONFORME**                                             | **CONFORME**                                                 |
  | ------------------------------------------------------------ | ------------------------------------------------------------ |
  | public function aVeryLongMethodName([-ClassTypeHint $arg1,-]<br />[-&$arg2, array $arg3 = []-])<br />{<br />&nbsp;   // method body<br />} | public function aVeryLongMethodName(<br />[+&nbsp;   ClassTypeHint $arg1,+]<br />[+&nbsp;   &$arg2,+]<br />[+&nbsp;   array $arg3 = []+]<br />) {<br />&nbsp;   // method body<br /> } |

  ------

- Quando a lista de **argumentos** de um **método** é dividida em várias linhas, o parêntese de fechamento e a chave de abertura devem ser colocados juntos em sua própria linha com um espaço entre eles.

  | **NÃO CONFORME**                                             | **CONFORME**                                                 |
  | ------------------------------------------------------------ | ------------------------------------------------------------ |
  | public function aVeryLongMethodName(ClassTypeHint $arg1,<br />&$arg2, array $arg3 = [] [-)-]<br />[-{-]<br />&nbsp;   // method body<br />} | public function aVeryLongMethodName(<br />&nbsp;   ClassTypeHint $arg1,<br />&nbsp;   &$arg2,<br />&nbsp;   array $arg3 = []<br />[+) {+]<br />&nbsp;   // method body<br /> } |

  ------

- Na lista de argumentos de uma **chamada de método ou função**, deve haver um espaço após cada vírgula.

  | **NÃO CONFORME**                                             | **CONFORME**                                                 |
  | ------------------------------------------------------------ | ------------------------------------------------------------ |
  | public static function doSomething([-$arg1,$arg2,$arg3-])<br />{<br />&nbsp;   //code<br />} | public static function doSomething($arg1,[+&nbsp;+]$arg2,[+&nbsp;+]$arg3)<br />{<br />&nbsp;   //code<br />} |

  ------

- As listas de argumentos de uma **chamada de método ou função** podem ser divididas em várias linhas, onde cada linha subsequente é recuada uma vez. Ao fazer isso, o primeiro item da lista deve estar na linha seguinte e deve haver apenas um argumento por linha.

  | **NÃO CONFORME**                                             | **CONFORME**                                                 |
  | ------------------------------------------------------------ | ------------------------------------------------------------ |
  | function aVeryLongFunctionName([-ClassTypeHint $arg1,-]<br />[-&$arg2, array $arg3 = []-])<br />{<br />&nbsp;   // method body<br />} | public function aVeryLongMethodName(<br />[+&nbsp;   ClassTypeHint $arg1,+]<br />[+&nbsp;   &$arg2,+]<br />[+&nbsp;   array $arg3 = []+]<br />) {<br />&nbsp;   // method body<br /> } |

  ------

- Na lista de argumentos de uma **chamada de método ou função**, não deve haver um espaço antes de cada vírgula.

  | **NÃO CONFORME**                                             | **CONFORME**                                                 |
  | ------------------------------------------------------------ | ------------------------------------------------------------ |
  | public static function doSomething($arg1{-&nbsp;-},$arg2{-&nbsp;-},$arg3)<br />{<br />&nbsp;   //code<br />} | public static function doSomething($arg1, $arg2, $arg3)<br />{<br />&nbsp;   //code<br />} |

  ------

- Ao fazer uma **chamada de método ou função**, não deve haver um espaço entre o método ou o nome da função e o parêntese de abertura, não deve haver um espaço após o parêntese de abertura e não deve haver um espaço antes do parêntese de fechamento.

  | **NÃO CONFORME**                                             | **CONFORME**                               |
  | ------------------------------------------------------------ | ------------------------------------------ |
  | bar{-&nbsp;-}({-&nbsp;-}$arg1{-&nbsp;-});<br />$foo->bar{-&nbsp;-}({-&nbsp;-}$arg1{-&nbsp;-}); | {+bar($arg1)+};<br />{+$foo->bar($arg1)+}; |

  ------

- Utilizar a estrutura de controle “**elseif**” em vez das estruturas “**else**” e “**if**” separadas.

  | **NÃO CONFORME**                                             | **CONFORME**                                                 |
  | ------------------------------------------------------------ | ------------------------------------------------------------ |
  | if ($condition1) {<br />&nbsp; &nbsp; // ...<br />} {-else if-} ($condition2) {<br />&nbsp; &nbsp; // ...<br />}<br /><br />if ($condition1) {<br />&nbsp; &nbsp; // ...<br />} {-else-} <br />&nbsp; &nbsp; {-if-} ($condition2) {<br />&nbsp; &nbsp; &nbsp; // ...<br />&nbsp; &nbsp; } | if ($condition1) {<br />&nbsp; &nbsp; // ...<br />} {+elseif+} ($condition2) {<br />&nbsp; &nbsp; // ...<br />} |

- Deve haver um espaço após as estruturas de controle “**if**”, “**elseif**”, “**else**”, “**switch**”, “**case**”, “**do**”, “**while**”, “**for**”, “**foreach**”, “**try**” e “**catch**”.

  | **NÃO CONFORME**                                             | **CONFORME**                                                 |
  | ------------------------------------------------------------ | ------------------------------------------------------------ |
  | {-if(-}$condition1){<br />&nbsp; &nbsp; // ...<br />}{-elseif(-}$condition2){<br />&nbsp; &nbsp; // ...<br />}{-else{-}<br />&nbsp; &nbsp; // ...<br />} | if{+&nbsp;+}($condition1) {<br />&nbsp; &nbsp; // ...<br />} elseif{+&nbsp;+}($condition2) {<br />&nbsp; &nbsp; // ...<br />} else{+&nbsp;+}{<br />&nbsp; &nbsp; // ...<br />} |

  ------

- Deve haver um espaço entre o parêntese de fechamento e a chave de abertura das estruturas de controle “**if**”, “**elseif**”, “**switch**”, “**while**”, “**for**”, “**foreach**” e “**catch**”.

  | **NÃO CONFORME**                                             | **CONFORME**                                                 |
  | ------------------------------------------------------------ | ------------------------------------------------------------ |
  | if($condition1{-){-}<br />&nbsp; &nbsp; // ...<br />}elseif($condition2{-){-}<br />&nbsp; &nbsp; // ...<br />}else{<br />&nbsp; &nbsp; // ...<br />} | if ($condition1){+&nbsp;+}{<br />&nbsp; &nbsp; // ...<br />} elseif ($condition2){+&nbsp;+}{<br />&nbsp; &nbsp; // ...<br />} else {<br />&nbsp; &nbsp; // ...<br />} |

  ------

- A chave de abertura deve permanecer na mesma linha das estruturas de controle “**if**”, “**elseif**”, “**else**”, “**switch**”, “**case**”, “**do**”, “**while**”, “**for**”, “**foreach**”, “**try**” e “**catch**”.

  | **NÃO CONFORME**                                             | **CONFORME**                                                 |
  | ------------------------------------------------------------ | ------------------------------------------------------------ |
  | {-if-} ($condition1)<br />{-{-}<br />&nbsp; &nbsp; // ...<br />}<br />{-elseif-}($condition2)<br />{-{-}<br />&nbsp; &nbsp; // ...<br />}<br />{-else-}<br />{-{-}<br />&nbsp; &nbsp; // ...<br />} | {+if+} ($condition1) {+{+}<br />&nbsp; &nbsp; // ...<br />} {+elseif+} ($condition2) {+{+}<br />&nbsp; &nbsp; // ...<br />} {+else+} {+{+}<br />&nbsp; &nbsp; // ...<br />} |

  ------

- Deve ser recuado uma vez o corpo das estruturas de controle “**if**”, “**elseif**”, “**else**”, “**switch**”, “**case**”, “**do**”, “**while**”, “**for**”, “**foreach**”, “**try**” e “**catch**”.

  | **NÃO CONFORME**                                             | **CONFORME**                                                 |
  | ------------------------------------------------------------ | ------------------------------------------------------------ |
  | if ($condition1) {-{// ...}-}<br />elseif ($condition2) {-{// ...}-}<br />else {-// ...-} | if ($condition1) {<br />{+&nbsp; &nbsp; +}// ...<br />} elseif ($condition2) {<br />{+&nbsp; &nbsp; +}// ...<br />} else {<br />{+&nbsp; &nbsp; +}// ...<br />} |

  ------

- A chave de fechamento deve estar na próxima linha após o corpo das estruturas de controle “**if**”, “**elseif**”, “**else**”, “**switch**”, “**case**”, “**do**”, “**while**”, “**for**”, “**foreach**”, “**try**” e “**catch**”.

  | **NÃO CONFORME**                                             | **CONFORME**                                                 |
  | ------------------------------------------------------------ | ------------------------------------------------------------ |
  | if ($condition1) {// ...{-}-}<br />elseif ($condition2) {// ...{-}-}<br />else <br />&nbsp; &nbsp; // ...<br />{-&nbsp;-} | if ($condition1) {<br />&nbsp; &nbsp; // ...<br />{+}+} elseif ($condition2) {<br />&nbsp; &nbsp; // ...<br />{+}+} else {<br />&nbsp; &nbsp; // ...<br />{+}+} |

  ------

- O corpo de cada estrutura de controle deve ser cercado por chaves. Isso padroniza a aparência das estruturas e reduz a probabilidade de introduzir erros à medida que novas linhas são adicionadas ao corpo.

  | **NÃO CONFORME**                                             | **CONFORME**                                                 |
  | ------------------------------------------------------------ | ------------------------------------------------------------ |
  | if ($condition1) {-&nbsp;-}<br />&nbsp; &nbsp; // ...<br />{-&nbsp;-}elseif ($condition2) {-&nbsp;-}<br />&nbsp; &nbsp; // ...<br />{-&nbsp;-}else {-&nbsp;-}<br />&nbsp; &nbsp; // ...<br />{-&nbsp;-} | if ($condition1) {+{+}<br />&nbsp; &nbsp; // ...<br />{+}+} elseif ($condition2) {+{+}<br />&nbsp; &nbsp; // ...<br />{+}+} else {+{+}<br />&nbsp; &nbsp; // ...<br />{+}+} |

  ------

- As estruturas de controle “**else**” e “**elseif**” devem estar na mesma linha que a chave de fechamento do corpo anterior.

  | **NÃO CONFORME**                                             | **CONFORME**                                                 |
  | ------------------------------------------------------------ | ------------------------------------------------------------ |
  | if ($condition1) {<br />&nbsp; &nbsp; // ...<br />{-}-} <br />{-elseif-} ($condition2) {<br />&nbsp; &nbsp; // ...<br />{-}-} <br />{-else-} {<br />&nbsp; &nbsp; // ...<br />} | if ($condition1) {<br />&nbsp; &nbsp; // ...<br />{+} elseif+} ($condition2) {<br />&nbsp; &nbsp; // ...<br />{+} else+} {<br />&nbsp; &nbsp; // ...<br />} |

  ------

- Não deve haver um espaço após o parêntese de abertura das estruturas de controle “**if**”, “**elseif**”, “**switch**”, “**while**”, “**for**”, “**foreach**” e “**catch**”.

  | **NÃO CONFORME**                                             | **CONFORME**                                                 |
  | ------------------------------------------------------------ | ------------------------------------------------------------ |
  | if ({-&nbsp;-}$condition1 ) {<br />   // ... <br />}  elseif ({-&nbsp;-}$condition2 ) {<br />   // ... <br />}  else {<br />   // ... <br />} | if {+($condition1)+} {<br />   // ... <br />} elseif {+($condition2)+} {<br />   // ... <br />} else {<br />   // ... <br />} |

  ------

- Não deve haver um espaço antes o parêntese de fechamento das estruturas de controle “**if**”, “**elseif**”, “**switch**”, “**while**”, “**for**”, “**foreach**” e “**catch**”.

  | **NÃO CONFORME**                                             | **CONFORME**                                                 |
  | ------------------------------------------------------------ | ------------------------------------------------------------ |
  | if ( $condition1{-&nbsp;-}) {<br />   // ... <br />}  elseif ( $condition2{-&nbsp;-}) {<br />   // ... <br />}  else {<br />   // ... <br />} | if {+($condition1)+} {<br />   // ... <br />} elseif {+($condition2)+} {<br />   // ... <br />} else {<br />   // ... <br />} |

  ------

- A palavra-chave “**var**” não deve ser utilizada para declaração de propriedades (atributos).

  | **NÃO CONFORME**                               | **CONFORME**                                      |
  | ---------------------------------------------- | ------------------------------------------------- |
  | class Foo {<br />     {-var-} $bar = 1;<br />} | class Foo {<br />     {+public+} $bar = 1;<br />} |

  ------

- Não deve haver mais de uma declaração de propriedade por linha.

  | **NÃO CONFORME**                                             | **CONFORME**                                                 |
  | ------------------------------------------------------------ | ------------------------------------------------------------ |
  | class Foo {<br />    {-private $bar = 1, $bar2 = 2;-}<br />} | class Foo {<br />    {+private $bar1 = 1;+}<br />    {+private $bar2 = 2;+}<br />} |

  ------

- As “**closures**” devem ser declaradas com um espaço depois da palavra-chave “**function**” e um espaço antes e depois da palavra-chave “**use**”.

  ------

- A chave de abertura deve vir na mesma linha da declaração da “**closure**” e a chave de fechamento deve vir na próxima linha após o corpo.

  ------

- Na lista de argumentos e na lista de variáveis da “**closure**”, deve haver um espaço depois de cada vírgula.

  ------

- Os argumentos de “**closure**” com valores padrão devem vir depois da lista de argumentos.

  ------

- As listas de argumentos e listas de variáveis de uma “**closure**” podem ser divididas em múltiplas linhas, onde cada linha subsequente é endentada uma vez. Quando fizer isso, o primeiro item na lista deve estar na próxima linha e deve haver um argumento ou variável por linha.

  ------

- Quando a lista de argumentos ou lista de variáveis de uma “**closure**” forem divididas em múltiplas linhas, o parêntese de fechamento e a chave de abertura DEVEM ser colocados na mesma linha com um espaço entre eles.

  ------

- Não deve haver espaço em branco no final de linhas com codificação.

  ------

- Não deve haver mais de uma **declaração por linha**.

  ------

- Na declaração de uma “**closure**” não deve haver um espaço após o parêntese de abertura de uma lista de argumentos ou lista de variáveis e não deve haver um espaço antes do parêntese de fechamento da lista de argumentos ou lista de variáveis.

  ------

- Na lista de argumentos e na lista de variáveis de uma “**closure**”, não deve haver um espaço antes de cada vírgula.

  ------

- Linhas em branco podem ser adicionadas para melhorar a legibilidade e para indicar blocos de código.

  ------

## Orientações

- Nomes de **propriedades** (atributos) devem seguir o **StudlyCaps**, **camelCase** ou **under_score**. Então para padronizar seguiremos o que diz na convenção Java que define o uso de **camelCase**.

  ------

- **Visibilidade** deve ser declarada para todas as **propriedades** (atributos).

  ------

- Nomes de classes (interfaces ou traits) devem ser declaradas em **StudlyCaps**

  ------

- **Constantes de classes** devem ser declaradas tudo em **MAIÚSCULO** com separador sendo **underscore**.

  ------

- Nomes de **métodos** devem ser declaradas em **camelCase** (não utilizar underscore).

  ------

- **Namespaces** e **classes** devem ser especificadas conforme implementação do **autoloading**.

  ------

- Nomes de **propriedades** **não** devem possuir um **prefixo** com um **sublinhado** para indicar visibilidade protegida ou privada.

  ------

- Nomes de **métodos** **não** devem possuir um **prefixo** com um **sublinhado** para indicar visibilidade protegida ou privada.

  ------

- Para recuos, usar sempre TAB representando o equivalente a 4 espaços. Não é permitido misturar com endentação por espaços. Está regra foi adotada para facilitar a padronização dos editores já que normalmente são os mesmos utilizados com a edição do React, que neste caso exige o uso o TAB no React Lint. Então neste caso iremos ir contra o que está descrito na PSR-2 (2.4).

  ------

- A instrução "**case**" deve ser recuada uma vez em relação ao "**switch**", e a palavra-chave "**break**" (ou outra palavra-chave de terminação) deve ser recuada no mesmo nível como o corpo do "case".

  ------

- Deve haver um comentário como "// no break" quando é intencional cair ("fall-through") num “**case**” com corpo não-vazio.

  ------

- Linhas não deveriam ter mais de **80 caractere**s. Caso ultrapasse esse limite é importante que sejam **divididas** em várias linhas subsequentes de no máximo 80 caracteres cada.

  ------

- Não deve haver um limite rígido no comprimento da linha.

  ------

- O **limite suave** no comprimento da linha deve ser de **120 caracteres**; Os verificadores de estilo automatizados devem avisar, mas não devem gerar um erro no limite suave.

## Regras extras (Não PSR)

- Estruturas de controle devem usar chaves. [https://rules.sonarsource.com/php/RSPEC-121]
- Parênteses não devem ser usados para chamadas para "echo". [https://rules.sonarsource.com/php/RSPEC-2041]
- Condicionais devem começar em novas linhas. [https://rules.sonarsource.com/php/RSPEC-3972]
- Linhas executada em uma condicional deve ser alinhada com um recuo. [https://rules.sonarsource.com/php/RSPEC-3973]
- Pares redundantes de parênteses devem ser removidos. [https://rules.sonarsource.com/php/RSPEC-1110]
- Funções devem usar o "**return**" consistentemente. [https://rules.sonarsource.com/php/RSPEC-3801]
- Declarações vazias devem ser removidas. [https://rules.sonarsource.com/php/RSPEC-1116]

## PHP CS Fixer

Site: [https://github.com/FriendsOfPHP/PHP-CS-Fixer]<br />
Arquivos: `Q:\desenvolvimento\PHP Code Formatter\PhpCsFixer`

#### Configurações para o VS Code

O arquivo original com as regras definidas está em `\php-cs-fixer\.php_cs.dist`.

```json
"php-cs-fixer.executablePath": "C:\\pasta_local_do_php-cs-fixer\\php-cs-fixer.phar",
"php-cs-fixer.config": "C:\\pasta_local_do_php-cs-fixer\\.php_cs.dist",
"php-cs-fixer.formatHtml": true,
"php-cs-fixer.autoFixBySemicolon": false,
```

#### Execução em linha de comando

1. Acessar a pasta `Q:\desenvolvimento\PHP Code Formatter\PhpCsFixer` e copiar para uma pasta na máquina local;
1. Instalar a biblioteca do Visual C++ 11 32bit, através do arquivo `vcredist_x86_vc11.exe`, para que seja possível executar o PHP 5.6;
1. Configurar as variáveis de ambiente "PHP_PATH" e "CSFIXER_PATH" com os caminhos da pastas "php5.6" e "php-cs-fixer" respectivamente. Caso não consiga ou não queira configurar essas variáveis de ambiente, configure diretamente no seu arquivo `\php-cs-fixer\csfix.bat` os respectivos caminhos;
1. Acrescente na variável de ambiente PATH o caminho so php-cs-fixer para que seja possível executar o comando "csfix" de qualquer pasta;
1. Para execução entre na pasta que `System\web\` e digite a linha de comando abaixo, mudando a pasta de acordo com o local desejado.

```
csfix include\problem
```

